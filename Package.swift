// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "WebDAV-neo",
    products: [
        .library(
            name: "WebDAV-neo",
            targets: ["WebDAV-neo"]),
    ],
    dependencies: [
        .package(url: "https://github.com/drmohundro/SWXMLHash.git", .upToNextMajor(from: "5.0.0"))
    ],
    targets: [
        .target(
            name: "WebDAV-neo",
            dependencies: ["SWXMLHash"]),
        .testTarget(
            name: "WebDAVTests",
            dependencies: ["WebDAV-neo"]),
    ]
)
